//index.js
//获取应用实例
const app = getApp()
Page({
  data: {
    channel:'37',
    address:''
  },
  //事件处理函数
  formSubmit: function(e) {
    const data  = e.detail.value;
    const address = data.address
    const channel = data.channel
    wx.setStorageSync('address', address)
    wx.setStorageSync('channel', channel)
    wx.navigateBack({
      delta: 1
    })
  },
  formReset(e) {
    this.setData({
      channel: '37',
      address:'',
      payload:''
    })
  },
  onLoad: function () {
    const channel = wx.getStorageSync('channel')||'37'
    const address = wx.getStorageSync('address')
    this.setData({
      channel,address
    })

  }
})
